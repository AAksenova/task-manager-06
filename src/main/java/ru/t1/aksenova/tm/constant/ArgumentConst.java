package ru.t1.aksenova.tm.constant;

public final class ArgumentConst {

    public static final String ABOUT = "-a";

    public static final String VERSION = "-v";

    public static final String HELP = "-h";

}
